import Background from '../components/Background'

export default class Hello extends Background {
    constructor() {
        super();
        this.background = new PIXI.Graphics();
        this.background.beginFill(0xDE3249);
        this.background.drawRect(50, 50, 100, 100);
        this.background.endFill();
        this.createBackground();
    }

}