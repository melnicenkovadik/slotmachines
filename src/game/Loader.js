import Preloader from '../components/Preloader';

export default class Loader extends Preloader {

  addImages() {
    //загрузка спрайтов
    this.loader.add([
      { name: 'BG', url: require("file!../assets/BG.png") },
      { name: 'BTN_Spin', url: require("file!../assets/BTN_Spin.png") },
      { name: 'BTN_Spin_d', url: require("file!../assets/BTN_Spin_d.png") },
      { name: 'line', url: require("file!../assets/Bet_Line.png") },
      { name: 'SYM1', url: require("file!../assets/SYM1.png") },
      { name: 'SYM2', url: require("file!../assets/SYM3.png") },
      { name: 'SYM3', url: require("file!../assets/SYM4.png") },
      { name: 'SYM4', url: require("file!../assets/SYM5.png") },
      { name: 'SYM5', url: require("file!../assets/SYM6.png") },
      { name: 'SYM6', url: require("file!../assets/SYM7.png") },
    ]);
  }
}